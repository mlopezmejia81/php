<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>CRUD</title>
    <link rel="stylesheet" type="text/css" href="style.php">
  </head>
  <body>
    <div class="fondo">
    <header>
      <small>Welcome to the</small>
      <h1>CRUD</h1>
      
    </header>

        <nav class="barras">
        <ul>
          <a class="nav-link" href="?opc=alta"><li>Alta</li></a>
          <a class="nav-link" href="?opc=baja"><li>Baja</li></a>
          <a class="nav-link" href="?opc=consulta"><li>Consulta</li></a>
          <a class="nav-link" href="?opc=modificacion"><li>Modificaciones</li></a>
        </ul>
      </nav>
    <div class="caja"></div>
    <section>
      <!--Codigo PHP para elegir opciones del CRUD-->
      <?php
      if (isset($_GET["opc"])) {
        switch ($_GET["opc"]) {
          case 'alta':
            include("alta.php");
            break;
          case 'baja':
            include("baja.php");
            break;
          case 'consulta':
            include("consulta.php");
            break;
          case 'modificacion':
            include("modificaciones.php");
            break;

        }
      }
       ?>
    </section>
    <!--Contenedor de imagenes para efectos de burbujas-->
    <div class="bubbles">
      
      <img src="img/bubble.png">
      <img src="img/bubble.png">
      <img src="img/bubble.png">
      <img src="img/bubble.png">
      <img src="img/bubble.png">
      <img src="img/bubble.png">
      <img src="img/bubble.png">
      <img src="img/bubble.png">

    </div>
    </div>

  </body>
</html>
