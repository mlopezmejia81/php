<?php
	header("Content-type:text/css; charset: UTF-8");

 ?>

*{
	margin:0;
	padding:0;
  font-familiy: sans-serif;
}
body{
 	  background-image: url("img/background.png");
 	  background-size: cover;
    width:100%;
    height:100vh;
    background-position: center;
    overflow: hidden;
    font-size: 16px;
   
}

header{
	color:white;
	padding: 0.5em;/* es para poner un espacio hacia adentro del contenedor y el madding es para afuera */
	text-align:center;
	width:100%;
}

legend{
  color:white;
  font-weight: bold;
}

label{
    color:#f5f5f5;
  font-weight: bold;
}
h1{
  font-size: 70px;
  margin: 5px 0 5px;
  line-height: 80px;
}
.barras {

  background-color: rgba(255, 255, 255, 0.3); 
  text-align: center;
  width:100%;
  padding: 1.5em;
}
a{
	text-decoration:none;
	padding:1.2em;
}
a:hover {
background-color: #f5f5f5;
opacity: 0.5;
}

ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
}

li {
  display: inline;/*para poner los elementos de navegacion en horizontal*/
  padding: 3em;
  madding: 0.5em;
  font-weight: bold;
  color: #808080;
  margin:0.5em auto;
}
section{	
  
  text-align: center;
  padding:40px;
  width:100%;
}

fieldset{
	text-align:center;
	padding:20px;
	width:90%;
	font-size:1.6em;
}

input[type="text"]{
	padding: 12px 10px;
	margin:0.5em;
	font-size:0.6em;
	text-align:left;
}

input[type=text]:focus {
   background-color: lightblue;
}

input[type="submit"]{
  border-radius: 4px;
  background-color: #696969;
  border: none;
  color: #FFFFFF;
  text-align: center;
  padding: 20px;
  width: 200px;
  transition: all 0.5s;
  cursor: pointer;
  opacity: 0.8;
  font-weight: bold;
}

table{
	font-weight:bold;
	width: 90%;
	border-collapse: collapse;
  border: 0px hidden  #ddd;
}
table .rows{
	background-color: #008080;
	opacity: 0.9;
	border: 0px hidden  #eee;
}
table td{
	border: 0px hidden  #ddd;
	padding:1.02em;
	text-align:left;
	border-bottom: 1px solid #ddd;
	background-color: #f5f5f5;
	opacity: 0.5;
}

tr:hover {
background-color: #f5f5f5;
opacity: 0.;
}

/*------------------EFECTO DE BURBUJA-------------------------*/
.select-css {
    font-size: 16px;
    font-weight: 700;
    color: #444;
    padding: .6em 1.4em .5em .8em;
    width: 50%;
    opacity: 0.8;
}

.bubbles img{
  width:50px;
  animation:bubble 7s linear infinite;
}

.bubbles{
  width:100%;
  display: flex;
  align-items: center;
  justify-content:space-around;
  position: absolute;
  bottom:-70px;

}

@keyframes bubble{
      0%{
  transform: translateY(0);
  opacity:0;
    }
      50%{
  opacity:1;
    }
      70%{
  opacity:1;
    }
      100%{
  transform: translateY(-80vh);
  opacity:0;
    }
}

.bubbles img:nth-child(1){
  animation-delay:2s;
  width:24px;
}
.bubbles img:nth-child(2){
  animation-delay:1s;
  width:35px;
}
.bubbles img:nth-child(3){
  animation-delay:2s;
}
.bubbles img:nth-child(4){
  animation-delay:3s;
  width:18px;
}
.bubbles img:nth-child(5){
  animation-delay:4.5s;
  width:15px;
}
.bubbles img:nth-child(6){
  animation-delay:3s;
}
.bubbles img:nth-child(7){
  animation-delay:7s;
  width:40px;
}